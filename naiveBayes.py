#!/usr/local/bin/python
import nltk
import csv
from textblob.classifiers import NaiveBayesClassifier


if __name__ == '__main__':
	
	# fin = open('yob2013.txt','r')
	# fout = open('names.txt','w')
	# firstnames = [line.split(',')[0] for line in fin.readlines()]
	# firstnames = set(firstnames)
	# for firstname in firstnames:
	# 	fout.write(firstname + " Chassang\n")


	# reader = csv.reader(open('soc_title.csv','rU'),delimiter=',')
	# fout = open('titles.txt','w')
	# for row in reader:
	# 	fout.write(row[0]+"\n")

	with open('names_short.txt','r') as fin:
		train_neg = [(line.replace('\n',''),'neg') for line in fin.readlines()]

	with open('titles_short.txt','r') as fin:
		train_pos = [(line.replace('\n',''),'pos') for line in fin.readlines()]

	train = train_neg + train_pos
	print train
	# train = [
	#     ('I love this sandwich.', 'pos'),
	#     ('This is an amazing place!', 'pos'),
	#     ('I feel very good about these beers.', 'pos'),
	#     ('This is my best work.', 'pos'),
	#     ("What an awesome view", 'pos'),
	#     ('I do not like this restaurant', 'neg'),
	#     ('I am tired of this stuff.', 'neg'),
	#     ("I can't deal with this", 'neg'),
	#     ('He is my sworn enemy!', 'neg'),
	#     ('My boss is horrible.', 'neg')
	# ]

	# test = [
	#     ('The beer was good.', 'pos'),
	#     ('I do not enjoy my job', 'neg'),
	#     ("I ain't feeling dandy today.", 'neg'),
	#     ("I feel amazing!", 'pos'),
	#     ('Gary is a friend of mine.', 'pos'),
	#     ("I can't believe I'm doing this.", 'neg')
	# ]
	
	cl = NaiveBayesClassifier(train)
	print cl.classify("software development engineer at saleswise")  # "pos"

	