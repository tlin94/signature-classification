#!/usr/local/bin/python


import talon

if __name__ == '__main__':
	#talon.init()



	message = """Hello Tien-Ju,

	Saw your application for the University Graduate Software Engineer job posting and I'd like to set up a time discuss this further.  When would you be available to speak later this week? Let me know a couple of dates/times that work best for a quick 15-20 minute call.

	I've copied below some questions that will help streamline our conversation, do you mind filling these out before our call?  I've also copied below some helpful information about Youtube as a whole, please take the time to review this before our call and I'd be happy to answer any questions you may have.

	Looking forward to our call!

	Antonio
	
	Antonio Caminong |
	Technical Recruiter at Youtube |
	650-214-6244 |
	"""
	
	text, signature = talon.signature.bruteforce.extract_signature(message)
	print signature
	# text == "Wow. Awesome!"
	# signature == "--\nBob Smith"